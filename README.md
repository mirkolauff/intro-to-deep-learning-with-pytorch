## Intro to deep learning with PyTorch

These are the notebooks for the 2019 Udacity course [Intro to deep learning with PyTorch] (https://classroom.udacity.com/courses/ud188)

### Requiremetns
- Python 3.6.8
- [pyenv](https://github.com/pyenv/pyenv)
- [poetry](https://poetry.eustace.io/)

### Installation
```
poetry install
```

### Starting
From within this repository call:
```
poetry run jupyter notebook .
```
